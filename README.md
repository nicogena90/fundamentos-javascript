# Fundamentos JavaScript

Este proyecto busca explicar los fundamentos de js, tipos de variables, funciones, asincronismo, etc.
Tambien incluye diferentes ejemplos en codigo de estos conceptos.

# Variables

JavaScript es un lenguaje debilmente tipado, es decir, al definir variables no definimos el tipo sino que las variables pueden adoptar valores del tipo string o int o cualquier otro tipo a lo largo del codigo.

- Nota: Cuando definimos numeros decimales en js  se guardaran tipo 64 bits floating type, es decir, js no define los tipos entre integer, short, long, punto flotante como lo hacen otros lenguajes.

Por ejemplo:

```
let x = 0.2 + 0.1;         // x sera 0.30000000000000004
```

Se resuelve de la siguiente manera
```
let x = (0.2 * 10 + 0.1 * 10) / 10;       // x sera 0.3
```

O con el uso de funciones en casos que no se sepa el numero de decimales que existen (Math.round).

## Diferentes tipos de declaraciones

- var: Al declarar variables con var lo que hacemos es definir el alcancede la variable mas alla del bloque donde fue definida, por ejemplo:

```
if (condicion) {
    var respuesta = '';
}
```

La variable respuesta estara disponible fuera de if donde la declaramos.

- let: Al declarar variables con let lo que hacemos es limitar el alcance de la variable dentro del bloque donde fue definida, por ejemplo:

```
if (condicion) {
    let respuesta = '';
}
```

La variable respuesta no estara disponible fuera de if donde la declaramos.

- const: Este tipo de variable es muy parecido a let pero con la diferencia que no puede asignarse otro valor con el que fue declarada, es decir, se utiliza para valores que sabemos que no van a cambiar su valor sino que funcionaran como constantes.

- Nota: Como buena practica se suele utilizar solo let y const en los programas para tener un mejor manejo de variables y los diferentes lugares donde deben ser accedidas.

# Funciones

Dentro de js se puede definir las funciones de la siguiente manera:

```
function showName(name) {
    return `El nombre es ${name}`;
}
```

O con arrow functions que es asi:

```
const showName = (name) => {
    return `El nombre es ${name}`; 
}
```

En este caso tambien podemos decir que estamos asignando a una variable una funcion anonima (porque no tiene nombre).

# Objetos

Los objetos en js se definen de la siguiente manera:

```
let persona = {
    nombre: "Nicolas",
    edad: 30
}
```

Una manera practica que nos permite js para obtener las propiedades de un objeto es:

```
const { nombre } = persona
```

Para hacer una copia del objeto y por ejemplo agregar o editar alguna de sus propiedades se puede hacer lo siguiente:

```
return {
    ...persona, // Aca realizamos la copia el objeto en este caso persona
    edad: 31 // Y aca estamos modificando en la copia generada el campo edad
}
```

# Comparaciones

El == compara valores pero no tipo

1 == "1" => true

El === compara valor y tipo

1 === "1" => false

Este tipo de comparacion sera valido en tipos de datos primitivos

Existen cinco tipos de datos que son primitivos:

Boolean:// false/true
Null:// nulo o "vacío"
Undefined:// Cuando una variable es declarada pero aún no tiene un valor asignado 
Number:// 383839
String:// "esto es una string"```

En cambio con objetos no ocurrira lo mismo porque se comparara la referencia de memoria, por mas que los objetos tengan las mismas propiedades.

# Arrays

Dentro de js los array se definen de la siguiente manera:

```
let personas = [];
```

Dentro de las [] podemos guardar una coleccion del tipo que queramos, number, string, objects, etc.

- Operaciones dentro de los array

```
let personas = ["Nicolas", "Cristian", "Pablo", "Julieta", "Gaston", "Nahuel"]
```

Las siguientes funciones generan una copia del array original, es decir, no modifican el original.

- Filter

Nos permite obtener los elementos del array que cumplan con una condicion

```
let personasNombreConN = personas.filter( (persona) => persona.charAt(0) === 'N')
result => ["Nicolas", "Nahuel"]
```

- Map

Nos devuelve un nuevo array pero con los componentes transformados a traves de una funcion

```
let personasMayusculas = personas.map( (persona) => persona.toUpperCase() === 'N')
result => ["NICOLAS", "CRISTIAN", "PABLO", "JULIETA", "GASTON", "NAHUEL"]
```

- Reducer

Nos permite reducir, mediante una función que se aplica a cada uno de los elemento del array, todos los elementos de dicho array, a un valor único.

```
let numeros = [1, 3, 5, 7 ,8];
let sumatoria = numeros.reduce( (acum, numero) => acum + numero)
result => 24
```

# Clases/Prototipos

JS es lenguaje basado en prototipos y cada objeto creado es tiene una propiedad interna llamada [[Prototype]] que se puede utilizar para extender sus propiedad y funciones, en resumen, las clases son funciones cuya sintaxis tiene dos componentes: expresiones y declaraciones

```
class Persona {
    constructor(nombre) {
        this.nombre = nombre;
        console.log('Se instancio persona');
    }

    saludar() {
        console.log(`Hola, mi nombre es ${this.nombre}`)
    }
}

let nicolas = new Persona('Nicolas');
nicolas.saludar();
```

Acabamos de definir una clase con su constructor y una funcion, sin embargo otra manera de asignar funciones a clases es la siguiente y se puede ver que se hace referencia al objeto creado con la palabra this

```
Persona.prototype.saludar = function () { console.log(`Hola, mi nombre es ${this.nombre}`) }
```

Si quisieramos hacer este ejemplo con arrow functions

```
Persona.prototype.saludar = () => { console.log(`Hola, mi nombre es ${this.nombre}`) }
```

Esto no funciona porque el contexto de un arrow function es distinto al de una funcion comun, en una arrow functions this hace referencia a windows, por lo que la propiedad nombre no existe, en cambio en un function comun this hace referencia a la clase deonde estamos definiendo la function.

- Nota: Para la herencia de clases dentro de js se usa herencia prototipal, es decir, le asignamos un prototipo de un objeto a otro para que este conozca todas las propiedades y funciones de objeto que esta "heredando", para la herencia se utiliza el extends.

# Efecto de lado

Un problema muy comun que existe en js y en cualquier lenguaje es el efecto de lado, esto ocurre cuando modificamos un valor sin la intencion de hacerlo, por ejemplo modificando variables globales o en contextos que no desamos cambiar, la solucion mas clara para que no ocurra esto es crear una copia de la variable que estamos moficando.

```
const nicolas = {
    nombre: 'Nicolas',
    edad: 30
}

// Aca creamos una copia de persona utilizando los '...' y cambiando una de sus propiedades
const cumpleanosInmutable = persona => ({
    ...persona,
    edad: persona.edad + 1
})
```

# Asincronismo basico

js sólo puede hacer una cosa a la vez, sin embargo; es capaz de delegar la ejecución de ciertas funciones a otros procesos. Este modelo de concurrencia se llama EventLoop.

js delega en el navegador ciertas tareas y les asocia funciones que deberán ser ejecutadas al ser completadas. Estas funciones se llaman callbacks, y una vez que el navegador ha regresado con la respuesta, el callback asociado pasa a la cola de tareas para ser ejecutado una vez que JavaScript haya terminado todas las instrucciones que están en la pila de ejecución.

Si se acumulan funciones en la cola de tareas y js se encuentra ejecutando procesos muy pesados, el EventLoop quedará bloqueado y esas funciones pudieran tardar demasiado en ejecutarse.

Por ejemplo:

```
setTimeout(function(){ alert("Yo despues"); }, 0);
alert("Yo primero");
```

Output => 
"Yo primero"
"Yo despues"

En principio, cualquier tarea que se haya delegado al navegador a través de un callback, deberá esperar hasta que todas las instrucciones del programa principal se hayan ejecutado. Por esta razón el tiempo de espera definido en funciones como setTimeout, no garantizan que el callback se ejecute en ese tiempo exactamente, sino en cualquier momento a partir de allí, sólo cuando la cola de tareas se haya vaciado.

# Asincronismo con APIs

Como vimos recien en js reina el asincronismo, es decir, si yo le pido a una api unos datos, js va a seguir con el programa y cuando recien me llegue la respuesta de la api se ejecutara el callback, funcion que defini cuando el servidor me responda con los datos que le pedi.

Esto puede ser bueno o puede ser malo, yo puedo necesitar esos datos para seguir con el programa por lo que voy a necesitar que se detenga todo hasta obtener una respuesta.

# Promesas

Las promesas son valores que aun no conocemos pero se nos "promete" que se nos va a devolver una respuesta y nos ayuda a manejar que hacer cuando se nos devuelve esta respuesta con sus diferentes estados:

- pending
- fullfilled
- rejected

Tambien nos ayudan a encadenar diferentes peticiones en vez de estar anidadas y asi evitar un CallbackHell y hacer que el código sea mucho más legible y mantenible.

# Ejemplos en el codigo

Esta seccion de asincronismo es mucho mas facil verla en el codigo que en la teoria por lo que dentro del repositorio se encuentran los siguientes ejemplos:

- syncExamples: Aca veremos un ejemplo de llamadas sincronicas a una API y como resultado el orden en que se resuelven estas peticiones

- asyncExamples: Aca veremos un ejemplo de llamadas asincronicas a una API y como resulta en un CallbackHell

- promiseExample: Aca veremos un ejemplo de promesas y como podemos resolver el problema de un CallbackHell

- promiseAllExample: Aca veremos un ejemplo de como utilizar la funcion de .all() y poder resolver las promesas de manera paralela

- awaitExample: Aca veremos un ejemplo del uso de await y funciones async, el metodo mas usado para manejar el asincronismo de js

Para poder ejecutar estos ejemplos vamos a necesitar clonar el repositorio localmente y luego en la carpeta del package.json ejecutar el comando "npm i" para instalar la libreria de axios y luego para ejecutar cada programa usar el comando 
"node <nombre_del archivo>"

# Fuentes

https://medium.com/@ubykuo/event-loop-la-naturaleza-asincr%C3%B3nica-de-javascript-78d0a9a3e03d