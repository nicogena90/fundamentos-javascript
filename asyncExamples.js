// Aca tenemos un ejemplo de realizar peticiones asincronicamente, es decir, en orden segun queramos

const axios = require('axios');

// Api de star wars
// const API_URL = 'https://swapi.dev/api/';
// const PEOPLE_URL = 'people/'

// Api de pokemon
const API_URL = 'https://pokeapi.co/api/v2/';
const PEOPLE_URL = 'pokemon/'


// Funcion para loguear el nombre del personje
function logName({ name }) {
  console.log(`Hola soy ${name}`);
}

// Funcion para obtener 
function obtenerPersonaje(id, callback) {

  // Obtenemos los datos desde la API
  axios.get(`${API_URL}${PEOPLE_URL}${id}`)

  // El then lo utilizamos para definir que hacer cuando recibimos los resultados
  .then( ({ data }) => {

    // Verificamos si se nos envia un callback para ejecutar
    if (callback){
        callback(data);
    }
  })
    
}

// Este anidado de funciones e slo que se conoce como CallbackHell
obtenerPersonaje(1, (data) => {
      logName(data)

      // Estamos definiendo que vaya a ejecutar al segundo personaje recien cuando obtengamos el 1
      obtenerPersonaje(2, (data) => {
        logName(data)
        obtenerPersonaje(3, (data) => {
          logName(data)
          obtenerPersonaje(4, (data) => {
            logName(data)
          })
        })
      })
  }
)
