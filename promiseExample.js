const axios = require('axios');

// Api de star wars
// const API_URL = 'https://swapi.dev/api/';
// const PEOPLE_URL = 'people/'

// Api de pokemon
const API_URL = 'https://pokeapi.co/api/v2/';
const PEOPLE_URL = 'pokemon/'

function obtenerPersonaje(id) {
  return new Promise((resolve, reject) => {
    axios.
      get(`${API_URL}${PEOPLE_URL}${id}`)
      .then( ({ data }) => {
        resolve(data)
      })
      .catch( () => reject(id))
  })
}

// Podemos ver que gracias a las promesas no anidamos las diferentes peticiones que hacemos sino que las concatenamos
obtenerPersonaje(1)
  .then(personaje1 => {
    console.log(`El personaje 1 es ${personaje1.name}`)
    return obtenerPersonaje(2)
  })
  .then(personaje2 => {
    console.log(`El personaje 2 es ${personaje2.name}`)
    return obtenerPersonaje(3)
  })
  .then(personaje3 => {
    console.log(`El personaje 3 es ${personaje3.name}`)
    return obtenerPersonaje(4)
  })
  .then(personaje4 => {
    console.log(`El personaje 4 es ${personaje4.name}`)
    return obtenerPersonaje(5)
  })
  .then(personaje5 => {
    console.log(`El personaje 5 es ${personaje5.name}`)
    return obtenerPersonaje(6)
  })
  .then(personaje6 => {
    console.log(`El personaje 6 es ${personaje6.name}`)
    return obtenerPersonaje(7)
  })
  .then(personaje7 => {
    console.log(`El personaje 7 es ${personaje7.name}`)
  })
  .catch(onError)


function onError(id) {
  console.log(`Sucedió un error al obtener el personaje ${id}`)
}