const axios = require('axios');

// Api de star wars
// const API_URL = 'https://swapi.dev/api/';
// const PEOPLE_URL = 'people/'

// Api de pokemon
const API_URL = 'https://pokeapi.co/api/v2/';
const PEOPLE_URL = 'pokemon/'

async function obtenerPersonaje(id) {
  const { data } = await axios.get(`${API_URL}${PEOPLE_URL}${id}`);

  return data;
}

// Utilizando el await lo que logramos es evitar utilizar el .then y de esta manera tener un codigo mas claro
async function obtenerPersonajes() {
  const ids = [1, 2, 3, 4, 5, 6, 7]
  const promesas = ids.map(id => obtenerPersonaje(id))
  try {
    const personajes = await Promise.all(promesas)
    personajes.map( (personaje) => console.log(personaje.name) )
  } catch (id) {
    onError(id)
  }
}

obtenerPersonajes()


function onError(id) {
  console.log(`Sucedió un error al obtener el personaje ${id}`)
}