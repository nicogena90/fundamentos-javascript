const axios = require('axios');

// Api de star wars
// const API_URL = 'https://swapi.dev/api/';
// const PEOPLE_URL = 'people/'

// Api de pokemon
const API_URL = 'https://pokeapi.co/api/v2/';
const PEOPLE_URL = 'pokemon/'

function obtenerPersonaje(id) {
  return new Promise((resolve, reject) => {
    axios.
      get(`${API_URL}${PEOPLE_URL}${id}`)
      .then( ({ data }) => {
        resolve(data)
      })
      .catch( () => reject(id))
  })
}

const ids = [1, 2, 3, 4, 5, 6, 7]

/* 
  Al utilizar .all() lo que hacemos es esperar que se completen todas las promesas y una vez que pase eso devolvemos todos los 
  resultados juntos en vez de uno por uno a medida que lo recibimos
*/
const promesas = ids.map(id => obtenerPersonaje(id))
Promise
  .all(promesas)
  .then(personajes => { personajes.map( (personaje) => console.log(personaje.name) )})
  .catch(onError)


function onError(id) {
  console.log(`Sucedió un error al obtener el personaje ${id}`)
}